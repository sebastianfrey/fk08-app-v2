function parse(document) {
    var channel = document.getElementsByTagName('channel')[0];

    console.log('Channel:', channel);

    var feed = {
        url: channel.getElementsByTagNameNS("http://www.w3.org/2005/Atom", 'link')[0].getAttribute('href'),
        description: channel.getElementsByTagName('description')[0].innerHTML,
        title: channel.getElementsByTagName('title')[0].innerHTML,
        items: []
    };

    // Parse items
    var itemElements = channel.getElementsByTagName('item');

    for (var i = 0; i < itemElements.length; i++) {
        var itemElement = itemElements[i];

        // Element wrappers
        var titleElement = itemElement.getElementsByTagName('title')[0];
        var linkElement = itemElement.getElementsByTagName('link')[0];
        var descriptionElement = itemElement.getElementsByTagName('description')[0].childNodes[0];
        var contentElement = itemElement.getElementsByTagNameNS("http://purl.org/rss/1.0/modules/content/", 'encoded')[0].childNodes[0];
        // Item data
        var title = titleElement.innerHTML;
        var link = linkElement.innerHTML;
        var description = descriptionElement.data;
        var content = contentElement.data;

        // Item
        var item = {
            // date: date,
            title: title,
            link: link,
            content: content,
            description: description
            // image: {
            //     url: imageUrl,
            //     alt: imageAlt,
            //     title: imageTitle
            // }
        }

        feed.items.push(item);
    }

    return feed;
}

// Only for dev
var parser = new DOMParser();
var feed = parse(parser.parseFromString(document.getElementsByTagName('pre')[0].innerText, 'application/xml'))
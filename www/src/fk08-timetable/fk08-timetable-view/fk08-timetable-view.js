Polymer({
    is: 'fk08-timetable-view',

    properties: {
        diagDay: {
            type: Number,
            value: 0,
        },
        diagStart: {
            type: Number,
            value: 0,
            observer: '_startChanged'
        },
        diagEnd: {
            type: Number,
            value: 1,
        }
    },

    _toggleDialog: function () {
        this.$.dialogAddLesson.toggle();
    },

    _addLesson: function() {
        console.log(this.$.inName.value);
        this.addLesson(this.$.inName.value, this.$.inAbbr.value, this.$.inColor.value, [{day: this.diagDay, begin: this.diagStart, end: this.diagEnd}]);
    },

    _startChanged: function(event) {
        if (this.diagEnd <= this.diagStart) {
            this.diagEnd = this.diagStart + 1;
        }
    },

    addLesson: function (name, abbr, color, time) {
        for (var d = 0; d < time.length; d++) {
            var t = time[d];
            var lesson = document.createElement('div');

            lesson.className = 'lesson';
            lesson.style.backgroundColor = color;
            lesson.innerText = abbr;

            lesson.style.height = 'calc(' + (t.end - t.begin) + ' * (100% + 1px))';

            var row = t.begin;
            var cell = t.day + 1;
            console.log('Row[' + row + ', ' + cell + ']:', Polymer.dom(this.$.timetable).children[1]);
            // Polymer.dom(this.$.timetable.rows[time.begin - 1].cells[time.day]).appendChild(lesson);
            Polymer.dom(this.$.timetable).children[1].children[row].children[cell].appendChild(lesson);
        }
    }
});
# fk08-App

## Voraussetzungen
- [git](https://git-scm.com/downloads) client f�r die Versionskontrolle
- [node.js](https://nodejs.org/en/)
- bower im Terminal/Eingabeaufforderung `npm install -g bower`
- Polymer CLI im Terminal/Eingabeaufforderung `npm install -g polymer-cli`

Alternative [Anleitung](https://www.polymer-project.org/1.0/docs/tools/polymer-cli) zur Installation.

## Erstellen und Einrichten
- `mkdir fk08-App` erstelle Ordner und navigiere in ihn `cd fk08-App`
- `git init` Initialisiere git
- `git remote add origin https://bitbucket.org/Codepuree/fk08-app-v2` füge den remote pfad vom Repository zu git hinzu.
- `git clone https://bitbucket.org/Codepuree/fk08-app-v2` klone das Repository

## Starte den Test Server
- `cd fk08-app-v2/www` navigiere in den `www`-Ordner
- `bower install` downloade und installiere alle polymer Komponenten mit bower
- `polymer serve` starte den Test-Server
- [http://localhost:8080](http://localhost:8080) �ffne die Seite im Browser

## Verwendete IDE
[Visual Studio Code](https://code.visualstudio.com/?utm_expid=101350005-28.R1T8FshdTBWEfZjY0s7XKQ.0&utm_referrer=https%3A%2F%2Fwww.google.de%2F)

## Verwendete Plugins für Visual Studio Code
- [To Do Tasks ](https://marketplace.visualstudio.com/items?itemName=sandy081.todotasks)
- [yUML](https://marketplace.visualstudio.com/items?itemName=JaimeOlivares.yuml)